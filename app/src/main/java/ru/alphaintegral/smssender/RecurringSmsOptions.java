package ru.alphaintegral.smssender;

import android.content.Intent;

public class RecurringSmsOptions {
    public static final String SMS_PHONE_KEY = "phone";
    public static final String SMS_TEXT_KEY = "sms_text";
    public static final String SEND_INTERVAL_MINUTES_KEY = "interval_min";
    public static final String START_TIME_KEY = "start_time";
    public static final String ITERATION_KEY = "iteration";

    private String phoneNumber;
    private String text;
    private Integer intervalMin;
    private Long startTime;
    private Long iteration;

    public RecurringSmsOptions(String phoneNumber, String text, Integer intervalMin, Long startTime, Long iteration) {
        this.phoneNumber = phoneNumber;
        this.text = text;
        this.intervalMin = intervalMin;
        this.startTime = startTime;
        this.iteration = iteration;
    }

    public void setStartTime(Long startTime) {
        this.startTime = startTime;
    }

    public void setIteration(Long iteration) {
        this.iteration = iteration;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public String getText() {
        return text;
    }

    public Integer getIntervalMin() {
        return intervalMin;
    }


    public Long getStartTime() {
        return startTime;
    }

    public Long getIteration() {
        return iteration;
    }

    public static RecurringSmsOptions from(Intent i) {
        return new RecurringSmsOptions(i.getStringExtra(SMS_PHONE_KEY), i.getStringExtra(SMS_TEXT_KEY), i.getIntExtra(SEND_INTERVAL_MINUTES_KEY, -1),
                i.getLongExtra(START_TIME_KEY, -1L), i.getLongExtra(ITERATION_KEY, 0));
    }

    public static void inflateIntent(RecurringSmsOptions options, Intent i) {
        i.putExtra(SMS_PHONE_KEY, options.getPhoneNumber());
        i.putExtra(SMS_TEXT_KEY, options.getText());
        i.putExtra(SEND_INTERVAL_MINUTES_KEY, options.getIntervalMin());
        i.putExtra(START_TIME_KEY, options.getStartTime());
        i.putExtra(ITERATION_KEY, options.getIteration());
    }
}
