package ru.alphaintegral.smssender;

import android.app.*;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Color;
import android.os.Build;
import android.os.IBinder;
import android.util.Log;
import androidx.annotation.Nullable;
import androidx.core.app.NotificationCompat;
import androidx.core.app.TaskStackBuilder;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;

public class RecurringSmsService extends Service {
    public static final String TAG = RecurringSmsService.class.getSimpleName();
    public static final String PING = "ping";
    public static final String PONG = "pong";

    public static final String SMS_SENT_ACTION = "SMS_SENT_ACTION";
    public static final String SMS_DELIVERED_ACTION = "SMS_DELIVERED_ACTION";
    private RecurringSmsOptions options;
    private Intent timerIntent;
    private ServiceEchoReceiver echoReceiver;
    private SmsAlarmReceiver smsAlarmReceiver;

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public void onCreate() {
        IntentFilter filter = new IntentFilter(PING);
        LocalBroadcastManager mgr = LocalBroadcastManager.getInstance(this);
        mgr.registerReceiver(echoReceiver = new ServiceEchoReceiver(), filter);

        filter = new IntentFilter();
        filter.addAction(SmsAlarmReceiver.START);
        filter.addAction(SmsAlarmReceiver.STOP);
        mgr.registerReceiver(smsAlarmReceiver = new SmsAlarmReceiver(), filter);
    }

    private class ServiceEchoReceiver extends BroadcastReceiver {
        public void onReceive(Context context, Intent intent) {
            Log.i(TAG, "Ping received, sending Pong!");
            LocalBroadcastManager
                    .getInstance(RecurringSmsService.this)
                    .sendBroadcastSync(new Intent(PONG));
        }
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        options = RecurringSmsOptions.from(intent);
        options.setIteration(0L);
        options.setStartTime(System.currentTimeMillis());
        stopTimer();
//        Toast.makeText(this, "starting", Toast.LENGTH_SHORT).show();
        if (options.getIntervalMin() > 0 && !options.getPhoneNumber().isEmpty()) {
            startForeground(1, buildNotification());
            startTimer();
        }
        return START_REDELIVER_INTENT;
    }

    private void startTimer() {
        timerIntent = new Intent(SmsAlarmReceiver.START);
        RecurringSmsOptions.inflateIntent(options, timerIntent);
        LocalBroadcastManager
                .getInstance(this)
                .sendBroadcastSync(timerIntent);
    }

    private void stopTimer() {
        Log.i(TAG, "stopping service");
        Intent stop = new Intent(this, SmsAlarmReceiver.class);
        stop.setAction(SmsAlarmReceiver.STOP);
        LocalBroadcastManager
                .getInstance(this)
                .sendBroadcastSync(stop);
    }

    private Notification buildNotification() {
        new Intent(this, MainActivity.class);
        Intent resultIntent = new Intent(this, MainActivity.class);
        TaskStackBuilder stackBuilder = TaskStackBuilder.create(this);
        stackBuilder.addNextIntentWithParentStack(resultIntent);
        PendingIntent resultPendingIntent =
                stackBuilder.getPendingIntent(0, PendingIntent.FLAG_UPDATE_CURRENT);

        String channelId = "";
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            channelId = createNotificationChannel("sms_sending_service", "sms_rec");
        }
        return new NotificationCompat.Builder(this, channelId)
                .setContentTitle(getText(R.string.notification_title))
                .setContentText(String.format("Sending sms for: %s every %d mins", options.getPhoneNumber(), options.getIntervalMin()))
                .setSmallIcon(R.drawable.ic_stat_sms)
                .setContentIntent(resultPendingIntent)
                .setTicker(getText(R.string.notification_title))
                .build();
    }

    private String createNotificationChannel(String channelId, String channelName) {
        NotificationChannel chan = new NotificationChannel(channelId,
                channelName, NotificationManager.IMPORTANCE_NONE);
        chan.setLightColor(Color.BLUE);
        chan.setLockscreenVisibility(Notification.VISIBILITY_PRIVATE);
        NotificationManager service = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        service.createNotificationChannel(chan);
        return channelId;
    }

    @Override
    public void onDestroy() {
//        Toast.makeText(this, "destroying", Toast.LENGTH_SHORT).show();
        stopTimer();
        LocalBroadcastManager mgr = LocalBroadcastManager.getInstance(this);
        mgr.unregisterReceiver(echoReceiver);
        mgr.unregisterReceiver(smsAlarmReceiver);
        Log.i(TAG, "service destroyed");
        super.onDestroy();
    }

}
