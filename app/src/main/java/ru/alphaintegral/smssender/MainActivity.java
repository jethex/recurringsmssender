package ru.alphaintegral.smssender;

import android.Manifest;
import android.app.Activity;
import android.content.*;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.telephony.SmsManager;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashSet;
import java.util.Locale;

import static ru.alphaintegral.smssender.RecurringSmsService.PING;

@SuppressWarnings("SameParameterValue")
public class MainActivity extends AppCompatActivity {
    private static final int SMS_PERMISSION_CODE = 10;

    private boolean running;

    EditText mPhoneEdit;
    EditText mIntervalEdit;
    EditText mTextEdit;
    Button mSendButton;

    private void reportState(int resId) {
        reportState(getString(resId));
    }

    private void reportState(String text) {
        reportState(text, false);
    }

    private void reportState(int resId, boolean error) {
        reportState(getString(resId), error);
    }

    private void reportState(String text, boolean error) {
        final Date currentDate = Calendar.getInstance().getTime();
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.getDefault());
        String timestamp = sdf.format(currentDate);
        text = timestamp + " " + text;

        TextView statusText = findViewById(R.id.status_text);
        statusText.setTextColor(Color.parseColor(error ? "red" : "black"));
        statusText.setText(text);
    }

    private void registerReceivers() {
        // the SMS has been sent
        registerReceiver(new BroadcastReceiver() {
            @Override
            public void onReceive(Context arg0, Intent arg1) {
                switch (getResultCode()) {
                    case Activity.RESULT_OK:
                        ContentValues values = new ContentValues();
                        final String phoneNumber = mPhoneEdit.getText().toString();
                        final String text = mTextEdit.getText().toString();
                        values.put("address", phoneNumber);
                        values.put("body", text);
                        getContentResolver().insert(Uri.parse("content://sms/sent"), values);
                        reportState("SMS sent");
                        break;
                    case SmsManager.RESULT_ERROR_GENERIC_FAILURE:
                        reportState(R.string.generic_failure, true);
                        break;
                    case SmsManager.RESULT_ERROR_NO_SERVICE:
                        reportState(R.string.no_service, true);
                        break;
                    case SmsManager.RESULT_ERROR_NULL_PDU:
                        reportState(R.string.null_pdu, true);
                        break;
                    case SmsManager.RESULT_ERROR_RADIO_OFF:
                        reportState(R.string.radio_off, true);
                        break;
                }
            }
        }, new IntentFilter(RecurringSmsService.SMS_SENT_ACTION));

        // the SMS has been delivered
        registerReceiver(new BroadcastReceiver() {
            @Override
            public void onReceive(Context arg0, Intent arg1) {
                switch (getResultCode()) {
                    case Activity.RESULT_OK:
                        reportState("SMS delivered");
                        break;
                    case Activity.RESULT_CANCELED:
                        reportState("SMS not delivered", true);
                        break;
                }
            }
        }, new IntentFilter(RecurringSmsService.SMS_DELIVERED_ACTION));
    }

    public boolean isServiceRunning() {
        LocalBroadcastManager.getInstance(this).sendBroadcastSync(new Intent(PING));
        return serviceRunning;
    }

    private void startService(String phone, String text, Integer minInterval) {
        RecurringSmsOptions options = new RecurringSmsOptions(phone, text, minInterval, System.currentTimeMillis(), 0L);
        Intent intent = new Intent(this, RecurringSmsService.class);
        RecurringSmsOptions.inflateIntent(options, intent);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            startForegroundService(intent);
        } else {
            startService(intent);
        }
    }

    private void stopService() {
        Intent intent = new Intent(this, RecurringSmsService.class);
        stopService(intent);
    }

    private boolean serviceRunning;

    private BroadcastReceiver pong = new BroadcastReceiver() {
        public void onReceive(Context context, Intent intent) {
            serviceRunning = true;
        }
    };

    private void setUi(boolean running) {
        mPhoneEdit.setEnabled(!running);
        mIntervalEdit.setEnabled(!running);
        mTextEdit.setEnabled(!running);
        mSendButton.setText(running ? R.string.stop : R.string.start);
        reportState(running ? R.string.sending_started : R.string.sending_stopped);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        mPhoneEdit = findViewById(R.id.phone_number_edit);
        mIntervalEdit = findViewById(R.id.period_edit);
        mTextEdit = findViewById(R.id.text_edit);
        mSendButton = findViewById(R.id.send_button);

        LocalBroadcastManager.getInstance(this).registerReceiver(pong, new IntentFilter(RecurringSmsService.PONG));
        running = isServiceRunning();
        setUi(running);
        mSendButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (running) {
                    running = false;
                    setUi(false);
                    stopService();
                } else {
                    final int interval = Integer.parseInt(mIntervalEdit.getText().toString());
                    if (interval < 1 || interval > 1000) {
                        mIntervalEdit.setError(getString(R.string.period_error_message));
                        return;
                    }

                    final String phoneNumber = mPhoneEdit.getText().toString();
                    if (phoneNumber.isEmpty()) {
                        mPhoneEdit.setError(getString(R.string.phone_number_error_message));
                        return;
                    }

                    final String text = mTextEdit.getText().toString();
                    if (text.isEmpty()) {
                        mTextEdit.setError(getString(R.string.text_error_message));
                        return;
                    }
                    running = true;
                    setUi(true);
                    startService(phoneNumber, text, interval);
                }
            }
        });

        boolean smsNotGranted = ActivityCompat.checkSelfPermission(this, Manifest.permission.SEND_SMS) != PackageManager.PERMISSION_GRANTED;
        if (smsNotGranted) {
            mSendButton.setEnabled(false);
            reportState(R.string.no_permission_to_send_sms, true);
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                HashSet<String> permissions = new HashSet<>();
                permissions.add(Manifest.permission.SEND_SMS);
                requestPermissions(permissions.toArray(new String[]{}), SMS_PERMISSION_CODE);
            }
        }
        registerReceivers();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        if (requestCode == SMS_PERMISSION_CODE) {
            final Button sendButton = findViewById(R.id.send_button);
            if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                sendButton.setEnabled(true);
                reportState(R.string.permission_granted);
            } else {
                sendButton.setEnabled(false);
                reportState(R.string.permission_denied, true);
            }
        }
    }

    @Override
    protected void onDestroy() {
        LocalBroadcastManager.getInstance(this).unregisterReceiver(pong);
        super.onDestroy();
    }
}
