package ru.alphaintegral.smssender;

import android.app.ActivityManager;
import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.telephony.SmsManager;
import android.util.Log;

import static android.app.AlarmManager.RTC_WAKEUP;
import static ru.alphaintegral.smssender.RecurringSmsService.SMS_DELIVERED_ACTION;
import static ru.alphaintegral.smssender.RecurringSmsService.SMS_SENT_ACTION;


public class SmsAlarmReceiver extends BroadcastReceiver {
    public static final String TAG = SmsAlarmReceiver.class.getSimpleName();
    public static final String START = "start";
    public static final String STOP = "stop";

    @Override
    public void onReceive(Context context, Intent intent) {
        AlarmManager alarmManager = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
        String action = intent.getAction();
        if (STOP.equals(action)) {
            cancel(context, alarmManager);
        } else {
            if (isMyServiceRunning(context)) {
                RecurringSmsOptions options = RecurringSmsOptions.from(intent);
                options.setIteration(options.getIteration() + 1);
                Intent next = new Intent(context, SmsAlarmReceiver.class);
                RecurringSmsOptions.inflateIntent(options, next);
                PendingIntent pi = PendingIntent.getBroadcast(context, 0, next, PendingIntent.FLAG_UPDATE_CURRENT);
                long triggerTime = options.getStartTime() + options.getIntervalMin() * options.getIteration()
                    * 60
                        * 1000;
                Log.i(TAG, "Next trigger time:" + triggerTime);
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                    AlarmManager.AlarmClockInfo ci = new AlarmManager.AlarmClockInfo(triggerTime, pi);
                    alarmManager.setAlarmClock(ci, pi);
                } else {
                    alarmManager.setExact(RTC_WAKEUP, triggerTime, pi);
                }
                sendSms(context, options.getPhoneNumber(), options.getText());
            } else {
//                Toast.makeText(context, "not running", Toast.LENGTH_SHORT).show();
            }
        }
    }

    private static boolean isMyServiceRunning(Context context) {
        ActivityManager manager = (ActivityManager) context.getSystemService(Context.ACTIVITY_SERVICE);
        for (ActivityManager.RunningServiceInfo service : manager.getRunningServices(Integer.MAX_VALUE)) {
            if (RecurringSmsService.class.getName().equals(service.service.getClassName())) {
                return true;
            }
        }
        return false;
    }

    public void cancel(Context context, AlarmManager alarms) {
        Intent i = new Intent(context, SmsAlarmReceiver.class);
        PendingIntent pi = PendingIntent.getBroadcast(context, 0, i, PendingIntent.FLAG_UPDATE_CURRENT);
        alarms.cancel(pi);
        Log.i(TAG, "stopping timed sms alarms");
//        Toast.makeText(context, "stopping timed sms alarms", Toast.LENGTH_SHORT).show();
    }

    private void sendSms(Context context, final String phoneNumber, final String text) {
        try {
            PendingIntent mSentIntent = PendingIntent.getBroadcast(context, 0, new Intent(SMS_SENT_ACTION), 0);
            PendingIntent mDeliveredIntent = PendingIntent.getBroadcast(context, 0, new Intent(SMS_DELIVERED_ACTION), 0);
            SmsManager sms = SmsManager.getDefault();
            sms.sendTextMessage(phoneNumber, null, text, mSentIntent, mDeliveredIntent);
            String msg = "sending sms:" + phoneNumber + " " + text + " " + System.currentTimeMillis();
//            Toast.makeText(context, msg, Toast.LENGTH_SHORT).show();
            Log.i(TAG, msg);
        } catch (Exception e) {
            final String message = e.getLocalizedMessage();
            if (message != null) {
                Log.e(TAG, message);
            } else {
                Log.e(TAG, "exception");
            }
        }
    }
}
